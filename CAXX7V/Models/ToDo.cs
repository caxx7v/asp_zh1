﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CAXX7V.Models
{
    public class ToDo
    {
        [Key]
        [StringLength(50)]
        public string ToDoID { get; set; }
        [StringLength(50)]
        [Display(Name = "Description")]
        public string Description { get; set; }      
        [Display(Name = "Employee")]
        public virtual Employee Employee { get; set; }
        [Required]
        [Display(Name = "Wage")]
        [Range(0,100000)]
        public int Wage { get; set; }
    }
}
