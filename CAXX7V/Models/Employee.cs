﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CAXX7V.Models
{
    public class Employee
    {
        [Key]
        [StringLength(50)]
        public string EmployeeID { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name="Name")]
        public string Name { get; set; }

        [NotMapped]
        [Display(Name = "Picture")]
        public IFormFile ImageFormData { get; set; }
        [StringLength(50)]
        public string ImageContentType { get; set; }
        public byte[] ImageData { get; set; }
    }
}
