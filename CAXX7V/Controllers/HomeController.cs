﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CAXX7V.Models;
using CAXX7V.Data;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CAXX7V.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext database;
        private UserManager<IdentityUser> userManager;
        private RoleManager<IdentityRole> roleManager;

        public HomeController(ApplicationDbContext database, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.database = database;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region ToDos
        [HttpGet]
        public IActionResult ToDos()
        {
            var todos = database.ToDos;
            return View(todos);
        }

        [HttpGet]
        [Authorize]
        public IActionResult CreateToDo()
        {
            var newtodo = new ToDo();
            var employees = database.Employees;
            ViewData["employees"] = employees;
            return View(newtodo);
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateToDo(ToDo todo, string employeeid)
        {
            var employee = database.Employees.FirstOrDefault(x => x.EmployeeID == employeeid);
            todo.Employee = employee;

            if (todo.ToDoID is null)
            {
                todo.ToDoID = Guid.NewGuid().ToString();
            }
            
            if (!ModelState.IsValid)
            {
                ViewData["employees"] = database.Employees;
                return View(todo);
            }
            
            database.ToDos.Add(todo);
            database.SaveChanges();

            return RedirectToAction(nameof(ToDos));
        }
        #endregion

        #region Employees
        [HttpGet]
        public IActionResult Employees()
        {
            var employees = database.Employees;
            return View(employees);
        }

        [HttpGet]
        [Authorize(Roles = "AdminRole")]
        public IActionResult CreateEmployee()
        {
            return View(new Employee());
        }

        [HttpPost]
        [Authorize(Roles = "AdminRole")]
        public IActionResult CreateEmployee(Employee newService)
        {
            if (newService.EmployeeID is null)
            {
                newService.EmployeeID = Guid.NewGuid().ToString();
            }
            if (!ModelState.IsValid)
            {
                return View(newService);
            }
            if (newService.ImageFormData != null)
            {
                newService.ImageData = new byte[newService.ImageFormData.Length];
                using (var stream = newService.ImageFormData.OpenReadStream())
                {
                    stream.Read(newService.ImageData, 0, (int)newService.ImageFormData.Length);
                }
                newService.ImageContentType = newService.ImageFormData.ContentType;
            }
            database.Employees.Add(newService);
            database.SaveChanges();

            return RedirectToAction(nameof(Employees));
        }

        #endregion

        #region Tools
        public FileContentResult GetImageData(string serviceid)
        {
            var service = database.Employees.FirstOrDefault(x => x.EmployeeID == serviceid);
            if (service.ImageData is null)
            {
                return new FileContentResult(new byte[0], "application/jpg");
            }
            return new FileContentResult(service.ImageData, service.ImageContentType);
        }

        [Authorize]
        public async Task<IActionResult> Delegate()
        {
            var myself = this.User;
            IdentityUser user = await userManager.GetUserAsync(myself);
            await roleManager.CreateAsync(new IdentityRole() { Name = "AdminRole" });
            await userManager.AddToRoleAsync(user, "AdminRole");

            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}
